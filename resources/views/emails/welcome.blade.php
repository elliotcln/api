<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Welcome Email</title>
</head>
<body>
    <h1>Pretioù ~ Bienvenue</h1>
    <p>Bienvenue sur <strong>Pretioù</strong>.<br/>
    Pour valider la création de votre compte, cliquez sur ce lien pour créer votre mot de passe et avoir accès à vos établissements.</p>
    <a target="_blank" href="localhost:3000/auth/reset_password?email={{ $user->email }}&token={{ $token }}">Initialiser mon compte sur Pretioù</a>
</body>
</html>