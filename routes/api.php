<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// auth
Route::post('login', 'AuthController@login')->name('auth.login');
Route::post('register', 'AuthController@register')->name('auth.register');
Route::post('logout', 'AuthController@logout')->name('auth.logout');

// users
Route::get('users', 'Api\UserController@index');
Route::put('users/{id}', 'Api\UserController@update');
Route::post('reset-password', 'Api\UserController@reset_password');
// Route::get('users/{id}/stores', 'Api\UserController@getUserStores')->name('users.getStores');
Route::delete('users/{id}', 'Api\UserController@delete');

// stores
Route::apiResource('stores', 'Api\StoreController');
Route::get('stores/{id}/menus', 'Api\StoreController@getStoreMenus')->name('stores.getMenus');

// stores modifications
Route::get('stores-modifications', 'Api\StoreModificationsController@index');
Route::post('stores-modifications', 'Api\StoreModificationsController@store');
Route::post('stores-modifications/{id}/approve', 'Api\StoreModificationsController@approve');
Route::delete('stores-modifications/{id}', 'Api\StoreModificationsController@destroy');

// menus
Route::apiResource('menus', 'Api\MenuController');

// files
Route::post('files', 'Api\FileController@storeFiles')->name('files.store');
Route::delete('files/{id}', 'Api\FileController@destroy')->name('file.destroy');

Route::post('qrcode/generate', 'QRCodeController@generate');

// tags
Route::get('tags', 'Api\TagController@index');
Route::post('tags', 'Api\TagController@store');
Route::delete('tags/{id}', 'Api\TagController@delete');

// google places
Route::get('googleplaces/{name}', 'Api\GooglePlacesController@getPlaces')->name('googleplaces.getPlaces');
Route::get('googleplaces/{place_id}/details', 'Api\GooglePlacesController@getPlaceDetails')->name('googleplaces.getPlaceDetails');
Route::get('googleplaces/nearbystores/{from}', 'Api\GooglePlacesController@getNearbyStores')->name('googleplaces.getNearbyStores');
