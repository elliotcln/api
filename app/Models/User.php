<?php

namespace App\Models;

use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * User has many stores
     *
     * @return Store
     */
    public function stores()
    {
        return $this->hasMany(Store::class);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'full_name',
        'email',
        'password',
        'role'
    ];

    public static function generatePassword()
    {
        // Generate random string and encrypt it. 
        return Str::random(35);
    }

    public static function sendWelcomeEmail($user)
    {
        // Generate a new reset password token
        // $token = app('auth.password.broker')->createToken($user);
        $token = bcrypt(Str::random(60));
        DB::insert('insert into password_resets (email, token) values (?, ?)', [$user->email, $token]);

        // Send email
        return response()->json([
            'token' => $token
        ]);
        // Mail::send('emails.welcome', ['user' => $user, 'token' => $token], function ($m) use ($user) {
        //     $m->from('kenavo@pretiou.fr', 'Pretioù');

        //     $m->to($user->email, $user->full_name)->subject('Bienvenue sur Pretioù');
        // });
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
