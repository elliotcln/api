<?php

namespace App\Http\Resources;

use App\Http\Resources\PlateResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MenuResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'store_id' => $this->store_id,
            'name' => $this->name,
            'plates' => json_decode($this->plates) 
        ];
    }
}
