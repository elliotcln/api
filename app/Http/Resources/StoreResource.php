<?php

namespace App\Http\Resources;

use App\Http\Resources\MenuResource;
use App\Http\Resources\UserResource;
use App\Http\Resources\FileResourceCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class StoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'published' => boolval($this->published),
            'infos' => json_decode($this->infos),
            'images' => FileResource::collection($this->files),
            'menus' => MenuResource::collection($this->menus),
            'user' => new UserResource($this->user),
            'updated_at' => $this->updated_at
        ];
    }
}
