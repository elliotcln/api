<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QRCodeController extends Controller
{

    private $rapidApiKey = '25e2bbaca0msh652ebe39bbe69bcp1e6276jsnd034815ab6f0';
    private $rapidApiHost = 'qrcode-monkey.p.rapidapi.com';

    public function generate(Request $request)
    {

        $url = 'https://url.com/' . $request->store['id'];
        $options = array(
            'data' => $url,
            'size' => '600',
            'file' => 'svg',
            'config' => array(
                'bodyColor' => '#0277BD',
                'body' => 'mosaic'
            )
        );
        $headers = array(
            'x-rapidapi-key' => $this->rapidApiKey,
            'x-rapidapi-host' => $this->rapidApiHost
        );

        // $qrcode = Http::withHeaders($headers)
        //     ->post('https://qrcode-monkey.p.rapidapi.com/qr/custom', $options);

        $png = QrCode::format('svg')->size(512)->generate($url);
        $qrcode = base64_encode($png);
        return response()->json(compact('qrcode'));
    }
}
