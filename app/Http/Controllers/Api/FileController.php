<?php

namespace App\Http\Controllers\Api;

use App\Models\Store;
use App\Models\File as FileApp;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{

    /**
     * Store multiple files
     *
     * @param Request $request
     * @return void
     */
    public function storeFiles(Request $request)
    {
        $files = $request->file('files');
        foreach($files as $file) {
            // $filename = time() . $file->getClientOriginalName();
            $path = $file->store('images', 's3');
            Storage::disk('s3')->setVisibility($path, 'public');
            // $path = Storage::disk('s3')->url($url);
            $f = FileApp::create([
                'store_id' => $request->store_id,
                'name' => basename($path),
                'path' => Storage::disk('s3')->url($path)
            ]);
        }
    }


    public function destroy($id)
    {
        $file = FileApp::where('id', $id)->first();
        
        if(File::exists($file)){
            unlink($file);
        }
        Storage::disk('s3')->delete('images/' . $file['name']);
        $file->delete();
    }

}
