<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Api\StoreController;
use App\Http\Resources\UserResourceCollection;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['reset_password']);
        // $this->middleware('guest')->only(['reset_password']);
    }

    /**
     * Get all users
     *
     * @return User
     */
    public function index(): UserResourceCollection
    {
        return new UserResourceCollection(User::all());
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @param User $user
     * @return User
     */
    public function update(Request $request): UserResource
    {

        $user = User::find($request->user['id']);

        // Gate::authorize('isAdmin' || 'isOwner', $user);

        $user->first_name = $request->user['first_name'];
        $user->last_name = $request->user['last_name'];
        $user->full_name = $request->user['first_name'] . ' ' . $request->user['last_name'];
        $user->email = $request->user['email'];

        if ($request->role) {
            $user->role = $request->role;
        }

        if (array_key_exists('password', $request->user)) {
            $user->password = Hash::make($request->user['password']);
        }

        $user->push();
        return new UserResource($user);
    }

    /**
     * Reset user password
     *
     * @param Request $request
     * @return UserResource $user
     */
    public function reset_password(Request $request)
    {
        //Validate input
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
            'token' => 'required'
        ]);

        //check if payload is valid before moving on
        if ($validator->fails()) {
            return response()->json(['error' => 'You need to complete the form.']);
            // return redirect()->back()->withErrors(['email' => 'Please complete the form']);
        }

        $password = $request->password;
        // Validate the token
        $tokenData = DB::table('password_resets')
            ->where('token', $request->token)->first();

        $user = User::where('email', $tokenData->email)->first();
        // Redirect the user back if the email is invalid
        if (!$user) return redirect()->back()->withErrors(['email' => 'Email not found']);
        //Hash and update the new password
        $user->password = Hash::make($password);
        $user->update(); //or $user->save();


        //Delete the token
        DB::table('password_resets')->where('email', $user->email)
            ->delete();

        return new UserResource($user);
    }

    public function delete($id)
    {

        Gate::authorize('isAdmin');

        $user = User::find($id);
        foreach ($user->stores as $user_store) {
            StoreController::destroy($user_store->id);
        }
        $user->delete();
    }
}
