<?php

namespace App\Http\Controllers\Api;

use App\Models\Tag;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TagController extends Controller
{

    /**
     * Get all Tags
     *
     * @return response
     */
    public function index() {
        $tags = Tag::all();
        return response()->json([
            'tags' => $tags
        ]);
    }

    public function store(Request $request) {
        $slugify = new Slugify();
        $tag = Tag::create([
            'title' => $request->title,
            'slug' => $slugify->slugify($request->title)
        ]);


        return response()->json(compact('tag'));
    }

    public function delete($id) {
        $tag = Tag::find($id);
        $tag->delete();
    }
}
