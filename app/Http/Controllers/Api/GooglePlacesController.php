<?php

namespace App\Http\Controllers\Api;

use App\Models\Store;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use App\Http\Resources\StoreResourceCollection;

class GooglePlacesController extends Controller
{
    private $google_api_key = 'AIzaSyA10DMgk8OLp2z7ChuaaxMe2-eh_cm_KKc';

    /**
     * Get the Places for $name
     *
     * @param String $name
     * @return Response $response
     */
    public function getPlaces($name)
    {
        $url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?key=' . $this->google_api_key . '&input=' . $name;
        $response = Http::get($url);

        return $response;
    }

    /**
     * Get the place details
     *
     * @param String $place_id
     * @return Response $response
     */
    public function getPlaceDetails($place_id)
    {
        $fields = 'name,address_components,international_phone_number,geometry/location,opening_hours,website';
        $url = 'https://maps.googleapis.com/maps/api/place/details/json?language=fr&place_id=' . $place_id . '&fields=' . $fields . '&key=' . $this->google_api_key;
        $response = Http::get($url);

        return $response;
    }


    /**
     * Get nearby stores
     *
     * @param [type] $from
     * @return StoreResourceCollection
     */
    public function getNearbyStores($from)
    {
        $stores = Store::where('published', true)->get();
        $nearbyStores = array();

        foreach ($stores as $store) {
            $infos = json_decode($store->infos);
            $lat = $infos->positions->lat;
            $lng = $infos->positions->lng;
            $to = $lat . ',' . $lng;

            $url = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=' . $from . '&destinations=' . $to . '&key=' . $this->google_api_key;
            $response = Http::get($url);

            $distance = $response['rows'][0]['elements'][0]['distance']['value'];
            if (isset($distance) && $distance < 5000) { // 5KM
                // $store['distance'] = $response['rows'][0]['elements'][0]['distance']['text'];
                array_push($nearbyStores, $store);
            }
        }

        return response()->json([
            'nearbyStores' => new StoreResourceCollection($nearbyStores)
        ]);
    }
}
