<?php

namespace App\Http\Controllers\Api;

use App\Models\File;
use App\Models\Menu;
use App\Models\Store;
use Cocur\Slugify\Slugify;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use App\Http\Resources\StoreResource;
use PhpParser\Node\Expr\Cast\Object_;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Api\FileController;
use App\Http\Resources\StoreResourceCollection;

class StoreController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['index', 'store', 'show']);
    }

    /**
     * Index function
     * Display a listing of the stores
     *
     * @return StoreResourceCollection
     */
    public function index()
    {
        $stores = Store::latest('updated_at')->paginate(11);
        return new StoreResourceCollection($stores);
    }

    /**
     * Store function
     *
     * @param Request $request
     * @return StoreResource
     */
    public function store(Request $request)
    {
        $slugify = new Slugify();
        $infos = json_decode($request->infos);
        $infos->slug = $slugify->slugify($infos->name . '-' . $infos->address_components->postal_code);

        $store = Store::create([
            'user_id' => $request->user_id,
            'infos' => json_encode($infos)
        ]);
        $files = $request->file('files');
       
        return response()->json([
            'store' => new StoreResource($store),
            'files' => $store->files->all()
        ]);
    }

    /**
     * Show function
     *
     * @param int $id
     * @return StoreResource
     */
    public function show(Store $store): StoreResource
    {
        return new StoreResource($store);
    }

    /**
     * Update function
     *
     * @param Request $request
     * @param Store $store
     * @return StoreResource
     */
    public function update(Request $request, Store $store)
    {

        $slugify = new Slugify();
        $infos = $request->infos;
        $infos['slug'] = $slugify->slugify($infos['name'] . '-' . $infos['address_components']['postal_code']);

        if (Gate::allows('isAdmin')) {
            $store->update([
                'user_id' => $request->user['id']
            ]);
        }

        $store->update([
            'published' => intval($request->published),
            'infos' => json_encode($infos)
        ]);

        if ($request->menus) {

            for ($i = 0; $i < count($request->menus); $i++) {

                $store->menus[$i]['name'] = $request->menus[$i]['name'];
                $store->menus[$i]['plates'] = json_encode($request->menus[$i]['plates']);
            }
        }

        $store->push();

        return new StoreResource($store);
    }

    /**
     * Destroy function
     *
     * @param int $id
     * @return void
     */
    public static function destroy($id)
    {
        $store = Store::find($id);
        $store->menus()->delete();
        $store->delete();
    }
}
