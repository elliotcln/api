<?php

namespace App\Http\Controllers\Api;

use App\Models\Menu;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\MenuResource;
use App\Http\Resources\MenuResourceCollection;

class MenuController extends Controller
{
    /**
     * Index function
     *
     * @return MenuResourceCollection
     */
    public function index() : MenuResourceCollection
    {
        return new MenuResourceCollection(Menu::paginate(10));
    }

    /**
     * Create function
     *
     * @param Request $request
     * @return MenuResource
     */
    public static function store(Request $request) : MenuResource
    {
        $menu = Menu::create($request->all());
        return new MenuResource($menu);
    }

    /**
     * Show function
     *
     * @param [type] $id
     * @return MenuResource
     */
    public function show(Menu $menu) : MenuResource
    {
        return new MenuResource($menu);
    }

    /**
     * Get Store Menus function
     *
     * @param int $id
     * @return MenuResourceCollection
     */
    public function getStoreMenus($id) : MenuResourceCollection
    {
        $menu = Menu::where('store_id', $id)->get();
        return new MenuResourceCollection($menu);
    }

    /**
     * Update function
     *
     * @param Request $request
     * @param Menu $menu
     * @return MenuResource
     */
    public function update(Request $request, Menu $menu) : MenuResource
    {
        $menu->update($request->except(['plates']));
        return new MenuResource($menu);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $menu = Menu::find($id);
        $menu->delete();
    }
}
