<?php

namespace App\Http\Controllers\Api;

use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Models\StoresModification;
use App\Http\Controllers\Controller;
use App\Http\Resources\StoreResource;

class StoreModificationsController extends Controller
{
    public function index()
    {
        /**
         * Destroy modifications older than a week
         */
        $old_modifications = StoresModification::where('created_at', '<=', Carbon::now()->subWeek())->get();
        foreach ($old_modifications as $md) {
            $this->destroy($md->id);
        }

        $modifications = StoresModification::all();

        return response()->json([
            'modifications' => $modifications
        ]);
    }

    public function store(Request $request)
    {
        $modification = StoresModification::create([
            "store_id" => $request->store_id,
            "modifications" => json_encode($request->modifications)
        ]);
        return response()->json(compact('modification'));
    }

    public function approve(Request $request) : StoreResource {
        $store = Store::find($request->store_id);

        // update modification state
        $modif = StoresModification::find($request->id);
        $modif->update([
            'state' => 1
        ]);

        $modif->save();

        return new StoreResource($store);
    }

    public static function destroy($id) {
        $modif = StoresModification::find($id);
        $modif->delete();
    }
}
