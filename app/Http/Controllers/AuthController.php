<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:sanctum')->except(['login']);
    }

    /**
     * Login function
     *
     * @param Request $request
     * @return void
     */
    public function login(Request $request)
    {
        $requestValidate = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        Auth::attempt($request->only('email', 'password'));
        $user = Auth::user();
        $token = $user->createToken('private-token');

        return response()->json([
            'user' => $user,
            'token' => $token->plainTextToken
        ]);
    }

    /**
     * Register function
     *
     * @param Request $request
     * @return void
     */
    public function register(Request $request)
    {
        Gate::authorize('isAdmin');

        $pw = User::generatePassword();

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'full_name' => $request->first_name . ' ' . $request->last_name,
            'email' => $request->email,
            'password' => $pw
        ]);

        $token = User::sendWelcomeEmail($user);

        // return response()->json(compact('user'), 200);
        return response()->json([
            'user' => new UserResource($user),
            'token' => $token->original['token']
        ]);
    }

    /**
     * Logout function
     *
     * @param Request $request
     * @return void
     */
    public function logout(Request $request)
    {
        $u = User::find($request->user['id']);
        $u->tokens()->delete();
        return response()->json([
            'message' => 'Logged Out'
        ]);
    }
}
